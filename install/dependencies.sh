sudo npm install -g jshint

case "$OSTYPE" in
darwin*)
    pip install neovim
    pip3 install neovim
;;
linux*)
    sudo pip install --user neovim
    sudo pip3 install --user neovim
;;
esac

