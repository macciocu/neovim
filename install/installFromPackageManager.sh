#!/usr/bin/env bash

# common files for software-properties like the D-Bus backend
sudo apt-get install -y software-properties-common

# prerequisites for the python modules
sudo apt-get install -y python-dev
sudo apt-get install -y python-pip
sudo apt-get install -y python3-dev
sudo apt-get install -y python3-pip

# neovim
pip3 install neovim

# syntastic
sudo npm install -g jshint
