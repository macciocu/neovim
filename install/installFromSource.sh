unzip neovim-0.2.0.zip
cd neovim-0.2.0

# build prerequisites Ubuntu / Debian
sudo apt-get install libtool libtool-bin autoconf automake cmake g++ pkg-config unzip
# build prerequisites CentOS/RHEL/Fedora
#sudo yum -y install libtool autoconf automake cmake gcc gcc-c++ make pkgconfig unzip
# build prerequisites OpenSUSE
#sudo zypper install libtool autoconf automake cmake gcc-c++ gettext-tools
# build prerequisites ArchLinux
#sudo pacman -S base-devel cmake unzip

make CMAKE_BUILD_TYPE=Release
sudo make install
cd ../
rm -rf ./neovim-0.2.0
