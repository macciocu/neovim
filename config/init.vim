" ************************************************************************************************
" File: init.vim
" Author: Giovanni Macciocu
" Notes:
"
" http://vim.wikia.com/wiki/Mapping_keys_in_Vim_-_Tutorial_%28Part_1%29
" inoreamp (mapping for insert mode)
" nnoremap (mapping for normal / command mode)
"
" to disable a command use:
" map X <nop>
"
" *** fu! ***
" Function declarations in Vimscript are runtime statements, so if a script is
" loaded twice, any fu! declarations in that script will be executed twice,
" re-creating the corresponding fu!s. Redeclaring a function is treated as
" a fatal error (to prevent collisions where two separate scripts accidentally
" declare fu!s of the same name). This makes it difficult to create functions
" in scripts that are designed to be loaded repeatedly, such as custom
" syntax-highlighting scripts. So Vimscript provides a keyword modifier (fu!!)
" that allows you to indicate that a fu! declaration may be safely reloaded
" as often as required:

" ************************************************************************************************ "
" ********************************************* Util ********************************************* "
" ************************************************************************************************ "

fu! OpenCppHeaderInVerticalSplit()
    let crt = expand('%:t:r')
    vnew
    exe 'tag '.crt.'.h'
endf

fu! ConvertTabs2spaces()
    set et
    ret!
endfu!

let s:doxy = 1
fu! ToggleDoxy()
    if s:doxy == 0
        let s:doxy = 1
        set formatoptions=ql
        echo "doxy off\n"
    else
        let s:doxy = 0
        set formatoptions=croq
        echo "doxy on\n"
    endif
endfu!

let s:toggleAllChars = 0
fu! ToggleAllChars()
    if s:toggleAllChars == 0
        let s:toggleAllChars=1
        set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
        set list
    else
        let s:toggleAllChars=0
        set nolist
    endif
endfu!

let s:cursorLineColor = 0
fu! ToggleCursorLineColor()
    if s:cursorLineColor == 1
        echo "disable cursorLineColor"
        let s:cursorLineColor=0
        if g:colors_name == "ndg" || g:colors_name == "nda" || g:colors_name == "ndr"
            hi CursorLine ctermbg=0 guibg=#151515
        else
            set nocursorline
        endif
    else
        echo "enable cursorLineColor"
        let s:cursorLineColor=1
        if g:colors_name == "morning"
            hi CursorLine ctermbg=7 guibg=#c0c0c0
        elseif g:colors_name == "evening"
            hi CursorLine ctermbg=016 guibg=#181818
        else
            hi CursorLine ctermbg=0 guibg=#000000
        endif
        set cursorline
    endif
endfu!

let s:indentMatch = 0
fu! ToggleIndentGuides()
 if s:indentMatch == 0
        let s:indentMatch = 1
        hi Indent guibg=#000000
        "match Indent /\%(\_^\s*\)\@<=\%(\%1v\|\%5v\|\%9v\)\s/
        match Indent /\%(\_^\s*\)\@<=\%(\%1v\|\%5v\|\%9v\|\%13v\|\%17v\|\%21v\|\%25v\|\%29v\)\s/
        echo "indent match on"
    else
        let s:indentMatch = 0
        match none
        echo "indent match off"
    endif
endfu!

fu! HelpCtags()
    echo "*** ctags help ***\n\n"
    echo "Ctrl + ]                  jump to definition\n"
    echo "Ctrl + w                  similar as above but opens result in new window pane\n"
    echo "Ctrl + t OR :pop          return after a tag jump\n"
    echo ":tag <tagname>            search available tags (supports tab completion)\n"
    echo ":stag <tagname>           similar as above but opens result in new window pane\n"
    echo ":tnext OR :tn             navigate to  next result in tag stack (in case of multiple matches)\n"
    echo ":tprevious OR :tp         similar as above but for previous result\n"
    echo ":tags                     show the contents of the tag stack (active entry is marked with >\n"
    echo ":<num>tag                 jump <num> forward in tag stack\n"
    echo ":help tag                 more help on ctags\n"
endfu!

fu! Explore()
    :e.|vs.|vs.|wincmd h|sp.|wincmd l|sp.|wincmd h|wincmd h
endfu

"delete the buffer; keep windows; create a scratch buffer if no buffers left
function s:Kwbd(kwbdStage)
  if(a:kwbdStage == 1)
    if(!buflisted(winbufnr(0)))
      bd!
      return
    endif
    let s:kwbdBufNum = bufnr("%")
    let s:kwbdWinNum = winnr()
    windo call s:Kwbd(2)
    execute s:kwbdWinNum . 'wincmd w'
    let s:buflistedLeft = 0
    let s:bufFinalJump = 0
    let l:nBufs = bufnr("$")
    let l:i = 1
    while(l:i <= l:nBufs)
      if(l:i != s:kwbdBufNum)
        if(buflisted(l:i))
          let s:buflistedLeft = s:buflistedLeft + 1
        else
          if(bufexists(l:i) && !strlen(bufname(l:i)) && !s:bufFinalJump)
            let s:bufFinalJump = l:i
          endif
        endif
      endif
      let l:i = l:i + 1
    endwhile
    if(!s:buflistedLeft)
      if(s:bufFinalJump)
        windo if(buflisted(winbufnr(0))) | execute "b! " . s:bufFinalJump | endif
      else
        enew
        let l:newBuf = bufnr("%")
        windo if(buflisted(winbufnr(0))) | execute "b! " . l:newBuf | endif
      endif
      execute s:kwbdWinNum . 'wincmd w'
    endif
    if(buflisted(s:kwbdBufNum) || s:kwbdBufNum == bufnr("%"))
      execute "bd! " . s:kwbdBufNum
    endif
    if(!s:buflistedLeft)
      set buflisted
      set bufhidden=delete
      set buftype=
      setlocal noswapfile
    endif
  else
    if(bufnr("%") == s:kwbdBufNum)
      let prevbufvar = bufnr("#")
      if(prevbufvar > 0 && buflisted(prevbufvar) && prevbufvar != s:kwbdBufNum)
        b #
      else
        bn
      endif
    endif
  endif
endfunction
command! Kwbd call s:Kwbd(1)

fu! DeleteHiddenBuffers()
    let tpbl=[]
    call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
    for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
        silent execute 'bwipeout' buf
    endfor
endfu!

fu! SetVerticalRulerOff()
    set colorcolumn&
endfu!

fu! SetVerticalRulerTo80()
    set colorcolumn=80
    hi ColorColumn guibg=#202020
endfu!

fu! SetVerticalRulerTo100()
    set colorcolumn=100
    hi ColorColumn guibg=#202020
endfu!

fu! SetVerticalRulerTo120()
    set colorcolumn=120
    hi ColorColumn guibg=#202020
endfu!

fu! InsertSection(word, length)
    if &filetype == "vim"
        let modlength = a:length - 6
        let a:inserted_word = ' ' . a:word . ' '
        let a:word_width = strlen(a:inserted_word)
        let a:length_before = (modlength - a:word_width) / 2
        let a:hashes_before = repeat('*', a:length_before)
        let a:hashes_after = repeat('*', modlength - (a:word_width + a:length_before))
        let a:hash_line = repeat('*', modlength)
        let a:word_line = a:hashes_before . a:inserted_word . a:hashes_after
        let a:hash_line = "\" *" . a:hash_line . "* \""
        let a:word_line = "\" *" . a:word_line . "* \""
        :put = a:hash_line
        :put = a:word_line
        :put = a:hash_line
    elseif &filetype == "python"
        let modlength = a:length - 2
        let a:inserted_word = ' ' . a:word . ' '
        let a:word_width = strlen(a:inserted_word)
        let a:length_before = (modlength - a:word_width) / 2
        let a:hashes_before = repeat('*', a:length_before)
        let a:hashes_after = repeat('*', modlength - (a:word_width + a:length_before))
        let a:hash_line = repeat('*', modlength)
        let a:word_line = a:hashes_before . a:inserted_word . a:hashes_after
        let a:hash_line = "# " . a:hash_line
        let a:word_line = "# " . a:word_line
        :put = a:hash_line
        :put = a:word_line
        :put = a:hash_line
    elseif &filetype == "cpp" || &filetype == "java"
        let modlength = a:length - 3
        let a:inserted_word = ' ' . a:word . ' '
        let a:word_width = strlen(a:inserted_word)
        let a:length_before = (modlength - a:word_width) / 2
        let a:hashes_before = repeat('*', a:length_before)
        let a:hashes_after = repeat('*', modlength - (a:word_width + a:length_before))
        let a:hash_line = repeat('*', modlength)
        let a:word_line = a:hashes_before . a:inserted_word . a:hashes_after
        let a:hash_line = "// " . a:hash_line
        let a:word_line = "// " . a:word_line
        :put = a:hash_line
        :put = a:word_line
        :put = a:hash_line
    elseif &filetype == "css"
        let modlength = a:length - 6
        let a:inserted_word = ' ' . a:word . ' '
        let a:word_width = strlen(a:inserted_word)
        let a:length_before = (modlength - a:word_width) / 2
        let a:hashes_before = repeat('*', a:length_before)
        let a:hashes_after = repeat('*', modlength - (a:word_width + a:length_before))
        let a:hash_line = repeat('*', modlength)
        let a:word_line = a:hashes_before . a:inserted_word . a:hashes_after
        let a:hash_line = "/* " . a:hash_line . " */"
        let a:word_line = "/* " . a:word_line . " */"
        :put = a:hash_line
        :put = a:word_line
        :put = a:hash_line
    endif
endfu!

fu! InsertLocalDate()
    :r! date "+\%c"
endfu!

fu! InsertUtcDate()
   r!LC_ALL=C date -u
endfu!

fu InsertDoxyHeader()
    let line00 = "/**"
    let line01 = " * @file " . expand('%:t')
    let line02 = " * @author Giovanni Macciocu"
    let line03 = " * @date " . strftime("%x %X %Z")
    let line04 = " */"
    :put = line00
    :put = line01
    :put = line02
    :put = line03
    :put = line04
endfu!

fu! Htm_tbl()
    let line00 = "<table>"
    let line01 = "    <tr>"
    let line02 = "        <td>"
    let line03 = "        </td>"
    let line04 = "    </tr>"
    let line05 = "</table>"
    :put = line00
    :put = line01
    :put = line02
    :put = line03
    :put = line04
    :put = line05
endfu!

fu! HorizontalTerm()
    20split
    term
endfu!

fu! VerticalTerm()
    vsplit
    term
endfu!

fu! EchoMyFunctions()
    echo "*** .vimrc help ***\n"
    echo "\n"
    echo ",db                     Delete hidden buffers.\n"
    echo "Tab2space               Convert all tabs to spaces.\n"
    echo "Doxy                    Toggle improved doxy editing.\n"
    echo "Chars                   Toggle all chars visibility.\n"
    echo "Ruler0                  Turn off vertical ruler.\n"
    echo "R80 / R100 / R120       Turn on vertical ruler at column 80 / 120.\n"
    echo "S100 / S80 / S60 / S40  Insert comment header section of width 80 / 60 / 40.\n"
    echo "Utc                     Insert UTC date.\n"
    echo "Date                    Insert local date.\n"
    echo "Dxh                     Insert doxygen formatted header.\n"
    echo "Htm_tbl                 Insert html table.\n"
    echo "\n"
    echo "<F1>                    Vertical terminal.\n"
    echo "<F2>                    Horizontal terminal.\n"
    echo "<F3>                    Toggle cursor line color.\n"
    echo "<F4>                    Toggle indent guide.\n"
    echo "<F5>                    Toggle ctags TagBar.\n"
    echo "<F6>                    ctags help.\n"
    echo "<F7>                    Calls Explore(), opens 5 files browsers in 1 window.\n"
    echo "<F12>                   Toggle paste-mode\n"
endfu!

" ************************************************************************************************ "
" ********************************************* Init ********************************************* "
" ************************************************************************************************ "

fu! InitFold()
    set foldmethod=indent
    set foldnestmax=3
    " automatic command (au), triggered when a buffer is read (BufRead), matching all files (*)
    " and executes the zR (opens all folds) command in normal mode.
    au BufRead * normal zR
endf

fu! InitIndent()
    " notes:
    "
    " *** smartindent ***
    " Avoid smartindent because it is depreciated in favor of cindent (when editing
    " a python file smartindent causes each # character to be placed at the first column)
    "
    " *** cindent ***
    " When it comes to C and C++, file type based indentations automatically
    " sets 'cindent', and for that reason, there is no need to set 'cindent' manually
    " for such files (Perhaps I need to use cindent for Java??)

    " copy the indentation from the previous line, when starting a new line.
    " autoindent does not interfere with other indentation settings.
    set autoindent

    " http://vimdoc.sourceforge.net/htmldoc/indent.html#cinoptions-values
    " disable the switch/case indent and indent labels at column 0
    "
    " I.e. this formats a switch / case code block as follows:
    " switch (k) {
    " case 0:
    "     ...
    " case 1:
    "     ...
    " }
    "
    set cinoptions+=:0,g0

    " size of a hard tabstop
    set tabstop=2
    " indentation without hard tabs (4 spaces for indenting)
    set expandtab
    set shiftwidth=2
    set softtabstop=2
endfu!

fu! InitSaveMyFingers()



    " ************************************ "
    " ********* normal-mode maps ********* "
    " ************************************ "
    nmap ; :
    nmap <space> :
    nmap f /
    nmap F ?

    " ************************************ "
    " ****** command-line-mode maps ****** "
    " ************************************ "

    cmap ,db call DeleteHiddenBuffers()
    cmap ,ls :BuffergatorOpen<CR>

    cmap ,ch call OpenCppHeaderInVerticalSplit()

    " allow saving when your fogot sudo
    cmap ,ws w !sudo tee % >/dev/null
    " save and execute Neomake for current file
    cmap ,wm w\|Neomake
    "delete the buffer; keep windows; create a scratch buffer if no buffers left
    cmap ,c Kwbd
    " + save
    cmap ,wc w\|Kwbd

    " find ctag and open in vertical split
    cmap ,cv vsp <CR>:exec("tag ".expand("<cword>"))<CR>
    " find ctag and open in horizontal split
    cmap ,ch sp <CR>:exec("tag ".expand("<cword>"))<CR>
    " find ctag and open in new tab
    cmap ,ct tab split<CR>:exec("tag ".expand("<cword>"))<CR>

    " ************************************ "
    " ********* insert-mode maps ********* "
    " ************************************ "
    imap ,, <Esc>

    " ************************************ "
    " ********* visual-mode maps ********* "
    " ************************************ "
    " smartpaste, 'p' to paste, 'gv' to re-select what was originally selected
    " and 'y' to copy it again
    xmap p pgvy

    " ************************************ "
    " **** terminal normal-mode maps ***** "
    " ************************************ "
    tnoremap <Esc> <c-\><c-n>

    " ************************************ "
    " ********** any-mode maps *********** "
	" ************************************ "
    " map ..

    " ************************************ "
    " ****** function key mappings ******* "
    " ************************************ "

    nnoremap <F1> :call VerticalTerm()<CR>
    nnoremap <F2> :call HorizontalTerm()<CR>
    nnoremap <F3> :call ToggleCursorLineColor()<CR>
    nnoremap <F4> :call ToggleIndentGuides()<CR>

    " TagbarToggle (plugin) shortkey
    nmap <F5> :TagbarToggle<CR>

    nmap <F6> :call HelpCtags()<CR>

    nmap <F7> :call Explore()<Cr>

    " Toggle paste option.
    "
    " If you use Vim commands to paste text, nothing unexpected occurs. The problem
    " only arises when pasting from another application, and only when you are not
    " using a GUI version of Vim.
    "
    " In a console or terminal version of Vim, there is no standard procedure to
    " paste text from another application. Instead, the terminal may emulate
    " pasting by inserting text into the keyboard buffer, so Vim thinks the text
    " has been typed by the user. After each line ending, Vim may move the cursor so
    " the next line starts with the same indent as the last. However, that will
    " change the indentation already in the pasted text.
    "
    " To paste from another application:
    "
    " Start insert mode.
    " Press F12 (toggles the 'paste' option on).
    " Use your terminal to paste text from the clipboard.
    " Press F12 (toggles the 'paste' option off).
    "
    nnoremap <F12> :set invpaste paste?<CR>

    " ************************************ "
    " ****** shorter function calls ****** "
    " ************************************ "

    command -nargs=* Tab2space call ConvertTabs2spaces()
    command -nargs=* Doxy call ToggleDoxy()
    command -nargs=* Chars call ToggleAllChars()

    command -nargs=1 R0 call SetVerticalRulerOff()
    command -nargs=1 R80 call SetVerticalRulerTo80()
    command -nargs=1 R100 call SetVerticalRulerTo100()
    command -nargs=1 R120 call SetVerticalRulerTo120()

    command -nargs=1 S100 call InsertSection(<f-args>, 100)
    command -nargs=1 S80 call InsertSection(<f-args>, 80)
    command -nargs=1 S60 call InsertSection(<f-args>, 60)
    command -nargs=1 S40 call InsertSection(<f-args>, 40)

    command -nargs=1 Utc call InsertUtcDate()
    command -nargs=1 Date call InsertLocalDate()
    command -nargs=1 Dxh call InsertDoxyHeader()
    command -nargs=1 Help call EchoMyFunctions()

    " disable vim's autocomment feature
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
endf

fu! InitNavigation()
    " cursor configuration
    " block cursor for normal, visual, command - mode
    set guicursor=n-v-c:block-Cursor
    " vertical line cursor for insert mode
    set guicursor+=i:ver25-Cursor/lCursor
    " turn of blinking in all modes
    set guicursor+=a:blink0

    "move mouse cursor to clicked position
    set mouse=a
    " When a bracket is inserted, briefly jump to the matching one.
    " The jump is only done if the match can be seen on the screen.
    set showmatch
    " make vsplit put the new buffer on the right of the current buffer
    " ps. <Ctrl-W r> swaps the windows
    set splitright

    " Ctrl + k will go one window up, Ctrl + h will go one window left,  etc.
    " (applies to vertical and horizontal split)
    nmap <silent> <C-k> :wincmd k<CR>
    nmap <silent> <C-j> :wincmd j<CR>
    nmap <silent> <C-h> :wincmd h<CR>
    nmap <silent> <C-l> :wincmd l<CR>
    " simalar as above, but now for terminal navigation
    tnoremap <C-Up> <C-\><C-n><C-w>h
    tnoremap <C-Down> <C-\><C-n><C-w>j
    tnoremap <C-Left> <C-\><C-n><C-w>k
    tnoremap <C-Right> <C-\><C-n><C-w>l

	  " Reduce the vertical size of the split by 10 columns
    "map <C-v> :vertical resize -10<CR>
    " Increase the vertical size of the split by 10 columns
    "map <C-c> :vertical resize +10<CR>
    " tab navigation
    nnoremap <C-u> :tabprevious<CR>
    nnoremap <C-i> :tabnext<CR>
    " cycle through listed buffers
    nnoremap <C-b> :bprevious<CR>
    nnoremap <C-n> :bnext<CR>

    " When copying wity 'y', it also copies to clipboard so it can be pasted
    " with cmd+v / ctrl+v
    set cb=unnamedplus

    " keep at least 5 lines above / below
    set scrolloff=5

    " never wrap lines
    set wrap!

    " Characters that form pairs.  The |%| command jumps from one to the other.
    " Currently only single byte character pairs are allowed, and they must be
    " different.
    set matchpairs=(:),{:},[:],<:>

    " The above command will change the 'completeopt' option so that Vim's popup menu
    " doesn't select the first completion item, but rather just inserts the longest
    " common text of all matches;
    set completeopt=longest,menuone

	" Cange the behavior of the <Enter> key when the popup menu is visible.
    " In that case the Enter key will simply select the highlighted menu item, just as <C-Y> does.
    inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endf

fu! InitAutoCorrect()
    " Automatically remove all trailing whitespace.
    " Every time the user issues a :w command, Vim will automatically remove all
    " trailing whitespace before saving
    " 'command' is need in order to jump back to 'mark' else vim moves the cursor
    " to each affected line, causing unwanted jump behavior.
    command! -range=% TR mark `|execute <line1> . ',' . <line2> . 's/\s\+$//'|normal! ``
    autocmd BufWritePre * :mark `|%s/\s\+$//e|normal! ``
endf

fu! InitAutomod()
    " Restore cursor position
    au BufReadPost *
        \ if line("'\"") > 0|
        \ if line("'\"") <= line("$")|
        \ exe("norm '\"")|
        \else|
        \exe "norm $"|
        \endif|
        \endif

    " Per default enable improved doxygen editing (This can be switched on / off
    " with the function Doxy()
    "
    " note: why not simply use 'set formatoptions=ql' (doxy off)? Because according
    " to the vim documentation:
    "   This option is set to the Vi default value when 'compatible' is
    "   set and to the Vim default value when 'compatible' is reset.
    "
    " even though whe've have 'set nompatible' some plugin might temporarily
    " change this (file plugins are loaded after the .vimrc), causing us to loose
    " our formatoption, this is suitable workaround for this problem.
    "
    autocmd BufNewFile,BufRead * setlocal formatoptions=ql

    " !!! This is not compatible with NERDTreeTabs plugin !!!
    " change the window-local current directory to be the same as the directory
    " of the current file (NB: don't use autochdir as this may cause problems
    " with plugins that make assumptions about the current directory
    autocmd BufEnter * silent! lcd %:p:h
endf

fu! InitSearch()
    " When 'ignorecase' and 'smartcase' are both on, if a pattern contains an
    " uppercase letter, it is case sensitive, otherwise, it is not. For example,
    " /The would find only \"The\", while /the would find \"the\" or \"The\" etc.
    " For example, you type / to initiate search, and right after you type the
    " letter a, vim will highlight the a in apple. As you type the next letter p,
    " vim will highlight ap in the word apple.
    set smartcase
    " incsearch stands for incremental search. It means that you will see what vim
    " matches as you type in each letter of your search string (without having to
    " hit return before search is even attempted).
    set incsearch
    " When there is a previous search pattern, highlight all its matches.
    set hlsearch
    "set ignorecase (just use this on the CLI, e.g. set ignorecase, set noignorecase)
endf

fu! InitStyle()
    " full color support palet
    " When on, uses |highlight-guifg| and |highlight-guibg| attributes in the terminal
    " (thus using 24-bit color). Requires a ISO-8613-3 compatible terminal.
    set termguicolors
    " enable syntax highlighting
    syntax on
    " display line numbers
    set number
    " highlight the line containing the cursor
    set cursorline

    " error highlight coloring (e.g. syntastic)
    hi SpellBad ctermfg=252 ctermbg=88 guifg=#c4c4c4 guibg=#b83737
    " warning highlight colore (e.g. syntastic)
    hi SpellCap ctermfg=252 ctermbg=94 guifg=#c4c4c4 guibg=#b85e37

    " show title in console title bar
    set title
    " always show statusbar
    set laststatus=2

    " [buffer nr.], absolute file path, modified flag (%F = full path, %f = relative path)
    set statusline+=[%n]%F%m
    " right align remainder
    "set statusline+=%=
    " lineNr, colNr, hexVal
    set statusline+=%l:%v\ %L\ 0x%B\ %*

    " netrw (file-explorer) - listing style
    " 0: thin, one file per line
    " 1: long, one file per line with file size and time stamp
    " 2: wide, which is files in columns
    " 3: tree style (NOT COMPATIBLE WITH WORD COMPLETION)
    let g:netrw_liststyle = 0
    " netrw (file-explorer) - hide header ('I' to show)
    let g:netrw_banner = 0

    if exists("g:theme_light")
      colorscheme morning
    else
      colorscheme evening
    endif
endf

fu! InitNeomake()
    " javascript
    let g:neomake_javascript_jshint_maker = {
        \ 'args': ['--verbose'],
        \ 'errorformat': '%A%f: line %l\, col %v\, %m \(%t%*\d\)',
    \ }
    let g:neomake_javascript_enabled_makers = ['jshint']

    " cpp
    let g:neomake_cpp_enabled_makers = ['gcc']
    let g:neomake_cpp_gcc_makers = {
        \ 'args': ['-fsyntax-only', '-Wall', '-Wextra', '-std=c++1y'],
        \ }

    " run Neomake on the current file on every write
    " Tip: use ':lopen' to open Neomake results list
    "autocmd! BufWritePost * Neomake
endfu!

" ************************************************************************************************ "
" For first time plugin install do take the following actions:
" 1) Install the plugins
" :PlugInstall
" 2) Restart
" ************************************************************************************************ "
" *** Command ***                       *** Description ***
" PlugInstall [name ..] [#threads]      Install plugins.
" PlugUpdate [name ..] [#threads]       Install or update plugins.
" PlugClean[!]                          Remove unused directories (bang version, cleans without prompt).
" PlugUpgrade                           Upgrade vim-plug itself.
" PlugStatus                            Check the status of the plugins.
" PlugDiff                              Examine changes for the previous update and the pending changes.
" PlugSnapshot[!]                       Generate script for restoring the current snapshot of the plugins.
" ************************************************************************************************ "
fu! InitPlugins()
    " set plugins directory (avoid using standard vim/nvim directory names like plugin)
    call plug#begin('~/.local/share/nvim/plugged')

    Plug 'neomake/neomake'
    " use the command 'ColorHighlight()' to highlight css colors
    Plug 'chrisbra/colorizer'
    " TAB autocompletion
    Plug 'ervandew/supertab'
    " buffer navigation
    Plug 'jeetsukumaran/vim-buffergator'
    " Set of tools for editing Csound files (.orc, .sco, .csd, .udo) with Vim: syntax recognition
    " and highlighting, folding, macros, autocompletion, on-line reference, and templates.
    Plug 'luisjure/csound'
    " Files with ANSI escape sequences look good when dumped onto a terminal that accepts them, but
    " have been a distracting clutter when edited via vim. The AnsiEsc.vim file, when sourced, will
    " conceal Ansi escape sequences but will cause subsequent text to be colored as the escape sequence
    " specifies.
    Plug 'powerman/vim-plugin-ansiesc'
    " React / JSX indenting and coloring
		Plug 'chemzqm/vim-jsx-improve'
    " init plugin system
    call plug#end()
endfu!

" ************************************************************************************************ "
" ********************************************* main ********************************************* "
" ************************************************************************************************ "

" autocompletion configuration (:h cpt)
setglobal complete=.,w,b,t

" turn off c-bracket mismatch (does not work correctly)
let c_no_curly_error=1

let g:python_host_prog="/usr/bin/python"
let g:python3_host_prog="/usr/bin/python3"

" mapping delay [ms]
set timeoutlen=500
" key code delay [ms
"set ttimeoutlen

" Turn backup and swapfile off, backup should be ensured through SVN or Git.
set nobackup noswapfile
" Make backspace work like most other apps
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" ctags (source code indexer) path
set tags=./.tags;

" if available load file specific plugin
filetype plugin on
" if available, load indenting file for specific file type
filetype indent on

call InitFold()
call InitIndent()
call InitSaveMyFingers()
call InitSearch()
call InitStyle()
call InitPlugins()

if exists("g:autocorrect_on")
    call InitAutomod()
    call InitAutoCorrect()
endif

call InitAutomod()
call InitNavigation()
call InitNeomake()

" END
