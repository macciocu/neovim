" @file javascript.vim
" @author Giovanni Macciocu
" @date 01/06/2019 23:22:11 CET

setlocal shiftwidth=2
setlocal tabstop=2
