" @file python.vim
" @author Giovanni Macciocu
"  @date 03/25/2019 00:41:05 CET

setlocal shiftwidth=4
setlocal tabstop=4
