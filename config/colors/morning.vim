" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=light
let g:colors_name="morning"

" Syntax
hi Normal         guibg=#f8dfce       guifg=#101010
hi Comment        guibg=None          guifg=#676767
hi String         guibg=None          guifg=#21177c
hi Constant       guibg=None          guifg=#647c3f " numbers
hi Identifier     guibg=None          guifg=#1f7871
hi Statement      guibg=None          guifg=#2d6c57
hi PreProc        guibg=None          guifg=#734b21
hi Type           guibg=None          guifg=#1e5b79
hi Special        guibg=None          guifg=#408c3e
hi Underlined     guibg=None          guifg=#454545
hi Ignore         guibg=None          guifg=#d24300
hi Error          guibg=None          guifg=#d10e44
hi! link Todo Error

" UI
hi LineNr         guibg=#f8dfce       guifg=#909090       gui=None
hi NonText        guibg=None          guifg=None          gui=None
hi SpecialKey     guibg=None          guifg=#3f4151       gui=None
hi Search         guibg=#0080e7       guifg=#aad9ff       gui=None
hi Visual         guibg=#3f4151       guifg=None          gui=None
hi PMenu          guibg=#004c8e       guifg=#ffffff       gui=None
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       gui=None
hi Directory      guibg=None          guifg=#687bb4       gui=None
hi Folded         guibg=#f8dfce       guifg=#7d7b77       gui=None
hi FoldColumn     guibg=#f8dfce       guifg=None          gui=None
hi CursorLine     guibg=#f8dfce       guifg=None          gui=None
hi CursorLineNr   guibg=#f8dfce       guifg=#8f2216       gui=None
hi VertSplit      guibg=#f8dfce       guifg=#808080       gui=None
hi StatusLine     guibg=#395a45       guifg=#91a78c       gui=None
hi StatusLineNC   guibg=#5a394e       guifg=#a87c8c       gui=None
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#f8dfce       guifg=#f8dfce
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#006000       guifg=#ffffff 
hi DiffDelete     guibg=#600000       guifg=#ffffff
hi DiffChange     guibg=#000060       guifg=#ffffff

hi MySemicolon    guifg=#0d00ca
hi MyColon        guifg=#8f2216
hi MyDot          guifg=#8f2216
hi MyComma        guifg=#161a8f
hi MyBrackets     guifg=#9c1f51
hi MySqBrackets   guifg=#c30000
hi MyCurlyBraces  guifg=#6f2543
hi MyOperators    guifg=#0066c3
