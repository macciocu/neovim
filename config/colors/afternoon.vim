" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=light
let g:colors_name="morning"

hi Normal         guibg=#c2bca8       guifg=#000000
hi Comment        guibg=None          guifg=#787878

" Syntax
hi Constant       guibg=None          guifg=#3f7fa1
hi Identifier     guibg=None          guifg=#a1613f
hi Statement      guibg=None          guifg=#3fa140
hi PreProc        guibg=None          guifg=#24720e
hi Type           guibg=None          guifg=#1774a0
hi Special        guibg=None          guifg=#888000
hi Underlined     guibg=None          guifg=#888000
hi Ignore         guibg=None          guifg=#d24300
hi Error          guibg=None          guifg=#d24300
hi! link Todo Error

" UI
hi LineNr         guibg=#c2bca8       guifg=#8a8883
hi NonText        guibg=None          guifg=None
hi SpecialKey     guibg=None          guifg=#3f4151
hi Search         guibg=#2c616c       guifg=#ffffff
hi TabLine        guibg=#595959       guifg=#adadad " non active tab label
hi TabLineFill    guibg=#595959       guifg=#595959 " tab pages line, where there are no labels
hi TabLineSel     guibg=#faffbf       guifg=#595959 " active tab
hi StatusLine     guifg=#595959       guibg=#f0f0f0 " bg and fg are reversed
hi StatusLineNC   guifg=#595959       guibg=#faffbf " bg and fg are reversed
hi Visual         guibg=#3f4151       guifg=None
hi Directory      guibg=None          guifg=#687bb4
hi MatchParen     guibg=#99485a       guifg=#ffffff    
hi Folded         guibg=#252525       guifg=#5f665f      
hi FoldColumn     guibg=#252525       guifg=None
hi CursorLine     guibg=#e3dfcf       guifg=None
hi VertSplit      guibg=#e0d0c0       guifg=#e3dfcf

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"

hi MySemicolon    guibg=None          guifg=#7c240f
hi MyColon        guibg=None          guifg=#ba7910
hi MyDot          guibg=None          guifg=#101884
hi MyComma        guibg=None          guifg=#1fba1d
hi MyOperators    guibg=None          guifg=#086b7d
hi MyBrackets     guibg=None          guifg=#1018d0
hi MySqBrackets   guibg=None          guifg=#13a4b9
hi MyCurlyBraces  guibg=None          guifg=#7d085f
