" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name="midnight"

" Syntax
hi Normal         guibg=#151515       guifg=#aeaeae
hi Comment        guibg=None          guifg=#767066
hi String         guibg=None          guifg=#8688b1
hi Constant       guibg=None          guifg=#f3c677 " numbers
hi Identifier     guibg=None          guifg=#d7bf8f
hi Statement      guibg=None          guifg=#60993e
hi PreProc        guibg=None          guifg=#b15331
hi Type           guibg=None          guifg=#5c9fb9
hi Special        guibg=None          guifg=#cfd186
hi Underlined     guibg=None          guifg=#454545
hi Ignore         guibg=None          guifg=#d24300
hi Error          guibg=None          guifg=#d10e44
hi! link Todo Error

" UI
hi LineNr         guibg=#151515       guifg=#505050       gui=None
hi NonText        guibg=None          guifg=None          gui=None
hi SpecialKey     guibg=None          guifg=#3f4151       gui=None
hi Search         guibg=#d10e44       guifg=#dddddd       gui=None
hi Visual         guibg=#3f4151       guifg=None          gui=None
hi PMenu          guibg=#004c8e       guifg=#ffffff       gui=None
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       gui=None
hi Directory      guibg=None          guifg=#687bb4       gui=None
hi Folded         guibg=#151515       guifg=#7d7b77       gui=None
hi FoldColumn     guibg=#151515       guifg=None          gui=None
hi CursorLine     guibg=#151515       guifg=None          gui=None
hi CursorLineNr   guibg=#151515       guifg=#eca400       gui=None
hi VertSplit      guibg=#151515       guifg=#454545       gui=None
hi StatusLine     guibg=#253f16       guifg=#71876c       gui=None
hi StatusLineNC   guibg=#4f2525       guifg=#876c6c       gui=None
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#151515       guifg=#ffffff       gui=None
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#006000       guifg=#ffffff 
hi DiffDelete     guibg=#600000       guifg=#ffffff
hi DiffChange     guibg=#000060       guifg=#ffffff

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"

hi MySemicolon    guifg=#6da9b8
hi MyColon        guifg=#e3655b
hi MyDot          guifg=#6da9b8
hi MyComma        guifg=#1fba1d
hi MyOperators    guifg=#086b7d
hi MyBrackets     guifg=#8fa7d7
hi MySqBrackets   guifg=#13a4b9
hi MyCurlyBraces  guifg=#8f8fd7
hi MyOperators    guifg=#ce6767
