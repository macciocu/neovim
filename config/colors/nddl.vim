" Default GUI Colours
let s:background      = "252525"
let s:foreground      = "929292"
let s:cursorline      = s:background
let s:linenr_bg       = s:background
let s:linenr_fg       = "3d4143"
let s:vertsplit       = "1d2123"
let s:cursorlinenr    = "3f889e"
let s:cursor_bg       = "555555"
let s:selection       = "3f4151"
let s:bracketmatch_fg = "202020"
let s:bracketmatch_bg = "99485a"
let s:non_text        = "af97e7"
let s:numbers         = "578c90"

let s:comment         = "5f665f"
let s:doxy            = s:comment

let s:todo            = "990000"
let s:string          = "768d39" " (C++) strings
let s:conditional     = "909000"
let s:typeof          = "687bb4" " (C++) type keywords + keywords
let s:include         = "107bb4"
let s:struct          = "687bb4" " (C++) (bash) keywords; (javascript) keywords
let s:identifier      = "6f8f91" " (C++) std methods; (bash) var declarations
let s:define          = "8d8539" " (C++) define + define keyword, include keyword
let s:precond         = "3f614b" " (C++) ifdef; (python) import related keywords
let s:typeof          = "6f8191" " (C++) type keywords + keywords
let s:search          = "2c616c"

let s:tab_bg               = s:background
let s:tab_fg               = "676767"
let s:tab_active_bg        = "253f16"
let s:tab_active_fg        = "71876c"
let s:statusline_active_bg = s:tab_active_bg
let s:statusline_active_fg = "71876c"
let s:statusline_bg        = "4f2525"
let s:statusline_fg        = "876c6c"

let s:diff_removed_bg = "600000"
let s:diff_removed_fg = "ffffff"
let s:diff_added_bg   = "006000"
let s:diff_added_fg   = "ffffff"
let s:diff_change_bg  = "000060"
let s:diff_change_fg  = "ffffff"

fu! LoadCustomSyntax()
  syn match operators /[?!=+%~|&*^<>-]/ | hi operators       guifg='#39848d'
  syn match brackets /[{}]/             | hi brackets        guifg='#687bb4'
  syn match curlybrackets /[()]/        | hi curlybrackets   guifg='#689bb4'
  syn match squaredbrackets /[\[\]]/    | hi squaredbrackets guifg='#9a8143'
  syn match dot /[.]/                   | hi dot             guifg='#2889a2'
  syn match comma /[,]/                 | hi comma           guifg='#a75252'
  syn match colon /[:]/                 | hi colon           guifg='#2985a2'
  syn match semicolon /[;]/             | hi semicolon       guifg='#b35627'
endf

" http://vimdoc.sourceforge.net/htmldoc/autocmd.html#autocmd-events
augroup CustomSyntaxAutomCmdGroup
  autocmd!
  autocmd BufEnter,BufNew,BufRead,ColorScheme,FileChangedShellPost,FileType * call LoadCustomSyntax()
augroup END

set background=dark
hi clear
syntax reset

let g:colors_name = "ndd"

" Returns an approximate grey index for the given grey level
fun <SID>grey_number(x)
  if &t_Co == 88
    if a:x < 23
      return 0
    elseif a:x < 69
      return 1
    elseif a:x < 103
      return 2
    elseif a:x < 127
      return 3
    elseif a:x < 150
      return 4
    elseif a:x < 173
      return 5
    elseif a:x < 196
      return 6
    elseif a:x < 219
      return 7
    elseif a:x < 243
      return 8
    else
      return 9
    endif
  else
    if a:x < 14
      return 0
    else
      let l:n = (a:x - 8) / 10
      let l:m = (a:x - 8) % 10
      if l:m < 5
        return l:n
      else
        return l:n + 1
      endif
    endif
  endif
endfun

" Returns the actual grey level represented by the grey index
fun <SID>grey_level(n)
  if &t_Co == 88
    if a:n == 0
      return 0
    elseif a:n == 1
      return 46
    elseif a:n == 2
      return 92
    elseif a:n == 3
      return 115
    elseif a:n == 4
      return 139
    elseif a:n == 5
      return 162
    elseif a:n == 6
      return 185
    elseif a:n == 7
      return 208
    elseif a:n == 8
      return 231
    else
      return 255
    endif
  else
    if a:n == 0
      return 0
    else
      return 8 + (a:n * 10)
    endif
  endif
endfun

" Returns the palette index for the given grey index
fun <SID>grey_colour(n)
  if &t_Co == 88
    if a:n == 0
      return 16
    elseif a:n == 9
      return 79
    else
      return 79 + a:n
    endif
  else
    if a:n == 0
      return 16
    elseif a:n == 25
      return 231
    else
      return 231 + a:n
    endif
  endif
endfun

" Returns an approximate colour index for the given colour level
fun <SID>rgb_number(x)
  if &t_Co == 88
    if a:x < 69
      return 0
    elseif a:x < 172
      return 1
    elseif a:x < 230
      return 2
    else
      return 3
    endif
  else
    if a:x < 75
      return 0
    else
      let l:n = (a:x - 55) / 40
      let l:m = (a:x - 55) % 40
      if l:m < 20
        return l:n
      else
        return l:n + 1
      endif
    endif
  endif
endfun

" Returns the actual colour level for the given colour index
fun <SID>rgb_level(n)
  if &t_Co == 88
    if a:n == 0
      return 0
    elseif a:n == 1
      return 139
    elseif a:n == 2
      return 205
    else
      return 255
    endif
  else
    if a:n == 0
      return 0
    else
      return 55 + (a:n * 40)
    endif
  endif
endfun

" Returns the palette index for the given R/G/B colour indices
fun <SID>rgb_colour(x, y, z)
  if &t_Co == 88
    return 16 + (a:x * 16) + (a:y * 4) + a:z
  else
    return 16 + (a:x * 36) + (a:y * 6) + a:z
  endif
endfun

" Returns the palette index to approximate the given R/G/B colour levels
fun <SID>colour(r, g, b)
  " Get the closest grey
  let l:gx = <SID>grey_number(a:r)
  let l:gy = <SID>grey_number(a:g)
  let l:gz = <SID>grey_number(a:b)

  " Get the closest colour
  let l:x = <SID>rgb_number(a:r)
  let l:y = <SID>rgb_number(a:g)
  let l:z = <SID>rgb_number(a:b)

  if l:gx == l:gy && l:gy == l:gz
    " There are two possibilities
    let l:dgr = <SID>grey_level(l:gx) - a:r
    let l:dgg = <SID>grey_level(l:gy) - a:g
    let l:dgb = <SID>grey_level(l:gz) - a:b
    let l:dgrey = (l:dgr * l:dgr) + (l:dgg * l:dgg) + (l:dgb * l:dgb)
    let l:dr = <SID>rgb_level(l:gx) - a:r
    let l:dg = <SID>rgb_level(l:gy) - a:g
    let l:db = <SID>rgb_level(l:gz) - a:b
    let l:drgb = (l:dr * l:dr) + (l:dg * l:dg) + (l:db * l:db)
    if l:dgrey < l:drgb
      " Use the grey
      return <SID>grey_colour(l:gx)
    else
      " Use the colour
      return <SID>rgb_colour(l:x, l:y, l:z)
    endif
  else
    " Only one possibility
    return <SID>rgb_colour(l:x, l:y, l:z)
  endif
endfun

" Returns the palette index to approximate the 'rrggbb' hex string
fun <SID>rgb(rgb)
  let l:r = ("0x" . strpart(a:rgb, 0, 2)) + 0
  let l:g = ("0x" . strpart(a:rgb, 2, 2)) + 0
  let l:b = ("0x" . strpart(a:rgb, 4, 2)) + 0

  return <SID>colour(l:r, l:g, l:b)
endfun

" Sets the highlighting for the given group
fun <SID>X(group, fg, bg, attr)
  if a:fg != ""
    exec "hi " . a:group . " guifg=#" . a:fg . " ctermfg=" . <SID>rgb(a:fg)
  endif
  if a:bg != ""
    exec "hi " . a:group . " guibg=#" . a:bg . " ctermbg=" . <SID>rgb(a:bg)
  endif
  if a:attr != ""
    exec "hi " . a:group . " gui=" . a:attr . " cterm=" . a:attr
  endif
endfun

" Vim Highlighting
call <SID>X("Normal", s:foreground, s:background, "none")
call <SID>X("LineNr", s:linenr_fg, s:linenr_bg, "")
call <SID>X("NonText", s:non_text, "", "")
call <SID>X("SpecialKey", s:selection, "", "")
call <SID>X("Search", s:background, s:search, "")
call <SID>X("TabLine", s:tab_fg, s:tab_bg, "none")
call <SID>X("TabLineFill", s:tab_bg, s:foreground, "")
call <SID>X("TabLineSel", s:tab_active_fg, s:tab_active_bg, "")
call <SID>X("StatusLine", s:statusline_active_fg, s:statusline_active_bg, "none")
call <SID>X("StatusLineNC", s:statusline_fg, s:statusline_bg, "none")
call <SID>X("VertSplit", s:vertsplit, s:linenr_bg, "none")
call <SID>X("Visual", "", s:selection, "")
call <SID>X("Directory", s:typeof, "", "")
call <SID>X("MatchParen", s:bracketmatch_fg, s:bracketmatch_bg, "")
call <SID>X("Folded", s:comment, s:background, "")
call <SID>X("FoldColumn", "", s:background, "")

call <SID>X("CursorLine", "", s:cursorline, "none")
call <SID>X("CursorColumn", "", s:cursorline, "none")
call <SID>X("PMenu", s:foreground, s:selection, "none")
call <SID>X("PMenuSel", s:foreground, s:selection, "reverse")
call <SID>X("SignColumn", "", s:background, "none")
call <SID>X("ColorColumn", "", s:cursorline, "none")

" Standard Highlighting
call <SID>X("CursorLineNr", s:cursorlinenr, "", "")
call <SID>X("Comment", s:comment, "", "")
call <SID>X("Todo", s:todo, s:background, "bold")
call <SID>X("Title", s:comment, "", "")
call <SID>X("Identifier", s:identifier, "", "none")
call <SID>X("Statement", s:struct, "", "none")
call <SID>X("Conditional", s:typeof, "", "")
call <SID>X("Repeat", s:struct, "", "")
call <SID>X("Structure", s:struct, "", "")
call <SID>X("Function", s:typeof,"","")
call <SID>X("Constant", s:numbers, "", "")
call <SID>X("String", s:string, "", "")
call <SID>X("Special", s:doxy, "", "")
call <SID>X("PreProc", s:precond, "", "")
call <SID>X("Operator", s:typeof, "", "none")
call <SID>X("Type", s:typeof, "", "none")
call <SID>X("Define", s:define, "", "none")
call <SID>X("Include", s:typeof, "", "")
call <SID>X("Ignore", s:define, "","")
call <SID>X("Number", s:numbers, "" , "")
  
" Vim Highlighting
" return, if, elseif ... belongs to vimCommand :(
call <SID>X("vimCommand", s:define, "", "none")

" C Highlighting
call <SID>X("cType", s:typeof, "", "")
call <SID>X("cStorageClass", s:struct, "", "")
call <SID>X("cConditional", s:typeof, "", "")
call <SID>X("cPreCondit", s:precond, "", "")
call <SID>X("cRepeat", s:struct, "", "")
call <SID>X("cDefine", s:define, "", "")
call <SID>X("cInclude", s:define, "", "")

" PHP Highlighting
call <SID>X("phpVarSelector", s:struct, "", "")
call <SID>X("phpKeyword", s:struct, "", "")
call <SID>X("phpIdentifier", s:typeof, "", "")
call <SID>X("phpType", s:identifier, "", "")
call <SID>X("phpOperator", s:struct,"","")
call <SID>X("phpRepeat", s:struct, "", "")
call <SID>X("phpConditional", s:typeof, "", "")
call <SID>X("phpStatement", s:struct, "", "")
call <SID>X("phpMemberSelector", s:typeof, "", "")
call <SID>X("phpStringSingle", s:numbers, "", "")
call <SID>X("phpDefine", s:define, "", "")
call <SID>X("phpStorageClass", s:typeof, "", "")
call <SID>X("phpStructure", s:typeof, "", "")
call <SID>X("phpParent", s:foreground, "", "")

" Python Highlighting
call <SID>X("pythonConditional", s:typeof, "", "")
call <SID>X("pythonRepeat", s:struct, "", "")
call <SID>X("pythonException", s:typeof, "", "")
call <SID>X("pythonStatement", s:struct, "", "")
call <SID>X("pythonImport", s:define, "", "")

" CoffeeScript Highlighting
call <SID>X("coffeeConditional", s:typeof, "", "")

" JavaScript Highlighting
call <SID>X("javaScriptBraces", s:foreground, "", "")
call <SID>X("javaScriptFunction", s:define, "", "")
call <SID>X("javaScriptNumber", s:numbers, "", "")
call <SID>X("javaScriptGlobal", s:struct, "", "")

" HTML Highlighting
call <SID>X("htmlTag", s:define,"","")
call <SID>X("htmlTagName", s:define,"","")
call <SID>X("htmlArg", s:define,"","")
call <SID>X("htmlScriptTag", s:define,"","")

" Diff Highlighting
call <SID>X("DiffAdd", s:diff_added_fg, s:diff_added_bg, "")
call <SID>X("DiffDelete", s:diff_removed_fg, s:diff_removed_bg, "")
call <SID>X("DiffChange", s:diff_change_fg, s:diff_change_bg, "")

" Delete Functions
delf <SID>X
delf <SID>rgb
delf <SID>colour
delf <SID>rgb_colour
delf <SID>rgb_level
delf <SID>rgb_number
delf <SID>grey_colour
delf <SID>grey_level
delf <SID>grey_number
