#!/usr/bin/env bash

lowercase(){
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

OS=`lowercase \`uname\``

if [[ "$OS" =~ ^msys ]] ||  [[ "$OS" =~ ^windows ]]; then
    rm -rf  "$HOMEPATH"/.vim
    mkdir "$HOMEPATH"/.vim
    cp -rv ./config/* "$HOMEPATH"/.vim/
    mv  "$HOMEPATH"/.vim/init.vim "$HOMEPATH"/.vimrc
else
    rm -r $HOME/.config/nvim
    mkdir $HOME/.config/nvim
    cp -rv ./config/* $HOME/.config/nvim/
fi