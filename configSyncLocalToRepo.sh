#!/usr/bin/env bash

cp -rvf $HOME/.config/nvim/* ./config/
rm -f ./config/doc/tags
